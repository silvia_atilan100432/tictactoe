/**
 * Tres en raya
 *
 * Temática: Tom y Jerry
 *
 * Integrantes: Silvia ATILANO
 *
 * Fecha: Abril 2020
 *
 */
#include "miniwin.h"

#include <iostream> // para std::cout, std::endl
using namespace std;
using namespace miniwin;

/*

 1 | 2 | 3
+--+---+--+
 4 | 5 | 6
+--+---+--+
 7 | 8 | 9

*/

// Variables globales
int juego_en_progreso;
char tablero[10] = { 'o', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
int turno_jugador;
int numero;
char marca;

// Funciones

void
dibujar_tablero();
int
verificar_juego();

void
dibujar_tablero()
{
  // TODO: AQUI es dónde tus compañeros deben de dibujar el tablero
  system("clear&&clear");
  cout << "\n\n\tTic Tac Toe\n\n";

  cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
  cout << endl;

  cout << "     |     |     " << endl;
  cout << "  " << tablero[1] << "  |  " << tablero[2] << "  |  " << tablero[3]
       << endl;

  cout << "_____|_____|_____" << endl;
  cout << "     |     |     " << endl;

  cout << "  " << tablero[4] << "  |  " << tablero[5] << "  |  " << tablero[6]
       << endl;

  cout << "_____|_____|_____" << endl;
  cout << "     |     |     " << endl;

  cout << "  " << tablero[7] << "  |  " << tablero[8] << "  |  " << tablero[9]
       << endl;

  cout << "     |     |     " << endl << endl;
}

/*********************************************
    La función regresa el estado del juego
    0 El juego está en progreso
    1 El juego se terminó con un ganador
    2 El juego se terminó sin ningún ganador
**********************************************/
int
verificar_juego()
{
  // Forma horizontal
  if (tablero[1] == 'X' and tablero[2] == 'X' and tablero[3] == 'X') {
    return 1;
  }
  else if (tablero[1] == 'O' and tablero[2] == 'O' and tablero[3] == 'O') {
    return 1;
  }
  else if (tablero[4] == 'X' and tablero[5] == 'X' and tablero[6] == 'X') {
    return 1;
  }
  else if (tablero[4] == 'O' and tablero[5] == 'O' and tablero[6] == 'O') {
    return 1;
  }
  else if (tablero[7] == 'X' and tablero[8] == 'X' and tablero[9] == 'X') {
    return 1;
  }
  else if (tablero[7] == 'O' and tablero[8] == 'O' and tablero[9] == 'O') {
    return 1;
  }

  // Forma vertical
  else if (tablero[1] == 'X' and tablero[4] == 'X' and tablero[7] == 'X') {
    return 1;
  }
  else if (tablero[1] == 'O' and tablero[4] == 'O' and tablero[7] == 'O') {
    return 1;
  }
  else if (tablero[2] == 'X' and tablero[5] == 'X' and tablero[8] == 'X') {
    return 1;
  }
  else if (tablero[2] == 'O' and tablero[5] == 'O' and tablero[8] == 'O') {
    return 1;
  }
  else if (tablero[3] == 'X' and tablero[6] == 'X' and tablero[9] == 'X') {
    return 1;
  }
  else if (tablero[3] == 'O' and tablero[6] == 'O' and tablero[9] == 'O') {
    return 1;
  }

  // Forma diagonal
  else if (tablero[1] == 'X' and tablero[5] == 'X' and tablero[9] == 'X') {
    return 1;
  }
  else if (tablero[1] == 'O' and tablero[5] == 'O' and tablero[9] == 'O') {
    return 1;
  }
  else if (tablero[7] == 'X' and tablero[5] == 'X' and tablero[3] == 'X') {
    return 1;
  }
  else if (tablero[7] == 'O' and tablero[5] == 'O' and tablero[3] == 'O') {
    return 1;
  }

  else if (tablero[1] == '1' or tablero[2] == '2' or tablero[3] == '3' or
      tablero[4] == '4' or tablero[5] == '5' or tablero[6] == '6' or
      tablero[7] == '7' or tablero[8] == '8' or tablero[9] == '9') {
    return 0;
  }

  return 2;
}

int
main()
{
  cout << "Bienvenido al juego del gato" << endl;
  juego_en_progreso = 1; // 0 = no, 1 = si
  turno_jugador = 1;

  // Mientras el juego esté en progreso
  while (juego_en_progreso == 1) {
    dibujar_tablero();
    // Si es el turno del jugador uno, entonces la marca es una X
    if (turno_jugador == 1) {
      cout << "Es el turno del jugador 1. " << "Ingresa un número" << endl;
      marca = 'X';
    }
    // Si es el turno del jugador uno, entonces la marca es una O
    else if (turno_jugador == 2) {
      cout << "Es el turno del jugador 2. " << "Ingresa un número" << endl;
      marca = 'O';
    }

    // Recuperar número que el jugador ingresó
    cin >> numero;

    // Poner X o O en la casilla que el jugador escogió
    if (numero == 1 && tablero[1] == '1')
    {
      tablero[1] = marca;
    } else if (numero == 2 && tablero[2] == '2')
    {
      tablero[2] = marca;
    }
    else if (numero == 3 && tablero[3] == '3')
    {
      tablero[3] = marca;
    }
    else if (numero == 4 && tablero[4] == '4')
    {
      tablero[4] = marca;
    }
    else if (numero == 5 && tablero[5] == '5')
    {
      tablero[5] = marca;
    }
    else if (numero == 6 && tablero[6] == '6')
    {
      tablero[6] = marca;
    }
    else if (numero == 7 && tablero[7] == '7')
    {
      tablero[7] = marca;
    }
    else if (numero == 8 && tablero[8] == '8')
    {
      tablero[8] = marca;
    }
    else if (numero == 9 && tablero[9] == '9')
    {
      tablero[9] = marca;
    }else
    {
      // TODO: error
    }

    int verifcar_juego_var = verificar_juego();

    cout << "Ver juego = " << verifcar_juego_var << endl;

    // El juego sigue en progreso
    if (verifcar_juego_var == 0)
    {
      juego_en_progreso = 1;
    }
    // El juego ha terminado con un ganador
    else if (verifcar_juego_var == 1)
    {
      dibujar_tablero();
      juego_en_progreso = 0;
      if (turno_jugador == 1) {
        cout << "Ha ganado el jugador 1" << endl;
      }
      if (turno_jugador == 2) {
        cout << "Ha ganado el jugador 2" << endl;
      }
    }
    // El juego ha terminado en un empate
    else if (verifcar_juego_var == 2)
    {
      dibujar_tablero();
      juego_en_progreso = 0;
      cout << "Nadie ganó =C es un empate" << endl;
    }
    

    if (turno_jugador == 1) {
      turno_jugador = 2;
    }else
    {
      turno_jugador = 1;
    }
    
  }
  cout << "Fin del juego" << endl;
  return 0;
}
